libtitanium-perl (1.04-5) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jaldhar H. Vyas from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Richard Hansen ]
  * Add myself to Uploaders
  * Delete unnecessary libcgi-application-perl version requirement
  * Move perl dependency from Build-Depends-Indep to Build-Depends
  * Annotate test-only dependencies with <!nocheck>
  * Add explicit dependency on libtest-simple-perl
  * Add copyright year to debian/copyright
  * Refresh patch via `gbp pq`
  * Delete obsolete lintian override
  * debian/patches/spelling.patch: More fixes
  * Set Rules-Requires-Root: no
  * Bump debhelper-compat to 13
  * Bump Standards-Version to 4.5.0
  * Add dependency on libcgi-application-plugin-debugscreen-perl

 -- Richard Hansen <rhansen@rhansen.org>  Sat, 22 Aug 2020 14:31:52 -0400

libtitanium-perl (1.04-4) unstable; urgency=low

  * Team upload.

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Fri, 05 Jun 2015 21:59:51 +0300

libtitanium-perl (1.04-3) unstable; urgency=low

  * Fixed typo in dependency

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 15 Oct 2011 10:24:27 +0100

libtitanium-perl (1.04-2) unstable; urgency=low

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Nicholas Bamber ]
  * Added myself to Uploaders
  * Raised debhelper version and compat level to 8
  * Raised standards version to 3.9.2
  * Updated copyright file
  * Updated dependencies to reflect split of
    libcgi-application-basic-plugin-bundle-perl
  * Added debian/source/format file
  * Added lintian override for DEP5 format
  * Added patch for spelling error

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 15 Oct 2011 01:05:51 +0100

libtitanium-perl (1.04-1) unstable; urgency=low

  * New upstream release
  * Add myself to Uploaders and Copyright
  * Now requires Compress-Raw-Zlib dependency
  * Standards-Version 3.8.3 (no changes)
  * Remove all build dependencies except Test::Pod
  * Refresh copyright information

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 30 Nov 2009 15:12:39 -0500

libtitanium-perl (1.03-1) unstable; urgency=low

  [ Nathan Handler ]
  * New upstream release
  * debian/watch:
    - Remove comments
    - Update to ignore development releases.
  * debian/control:
    - Add myself to list of Uploaders
    - Bump Standards-Version to 3.8.2
    - Do not begin synopsis with 'a'
  * debian/copyright:
    - Add year to upstream copyright
    - Add License-Alias

  [ gregor herrmann ]
  * Don't install README anymore (text version of the POD documentation).
  * Remove build dependency on libmodule-build-perl, debhelper prefers
    ExtUtils::MakeMaker; add build dependency on libtest-pod-perl.

 -- Nathan Handler <nhandler@ubuntu.com>  Mon, 29 Jun 2009 12:02:10 +0000

libtitanium-perl (1.01-1) unstable; urgency=low

  [ Jaldhar H. Vyas ]
  * New upstream version.
  * Switched to debhelper 7 and new format copyright.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Mon, 08 Dec 2008 18:17:46 -0500

libtitanium-perl (1.00-1) unstable; urgency=low

  * New upstream version.
  * First upload to Debian. (closes: #499357)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Fri, 19 Sep 2008 10:29:45 -0400

libtitanium-perl (0.90-1-1) unstable; urgency=low

  * Initial non-release.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Thu, 21 Aug 2008 19:25:52 -0400
